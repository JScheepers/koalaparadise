import './layout.css'
export const Layout = ({children}) => {
    return <div id="layout">
        {children}
    </div>
}