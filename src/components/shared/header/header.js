import React from "react";
import './header.css';

export default function Header({ 
    variant, 
    children, 
    width, 
    height,
    fontStyle,
    fontWeight,
    fontSize,
    marginBottom
}) {
    const element = variant ? variant : 'span';
    
    return React.createElement(
        element, 
        {
            style: { 
                width: width,
                height: height,
                fontStyle: fontStyle ?? "normal",
                fontWeight: fontWeight ?? "800",
                fontSize: fontSize ?? "18px",
                marginBottom: marginBottom ?? '8px',
            }
        }, 
        children
    )
}