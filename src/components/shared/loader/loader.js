import './loader.css';

export default function Loader({ message }) {
    return (
        <span className="loader">
            {message ? message : "Loading..."}
        </span>
    )
}