import React from "react";
import chevronRightInactive from '../../../images/chevron-right-inactive.svg';
import chevronRightActive from '../../../images/chevron-right-active.svg'
import Header from "../../shared/header/header";
import './koala-information.css'

const InformationHeader = ({children}) => 
    <Header
        variant="h3"
        fontWeight="600"
    > 
        {children}
    </Header>

export default function KoalaInformation({ koala, active, setActive }) {
 
    return (
        <div id="tab" className={active ? "activeTab" : "inactiveTab"} onClick={() => setActive(koala)}>
            <hr className={active ? "activeLine" : "inactiveLine"} />
            <InformationHeader>
                {koala.title}
            </InformationHeader>
            <p className="content">
                {koala.information}
            </p>
            <a className={active ? "activeLink" : "inactiveLink"} href={koala.url}>
                {koala.linkText}
                <img src={active ? chevronRightActive : chevronRightInactive} alt="chevron-right" className="chevronRight" />
            </a>
        </div>
    );
}