import NotFound from "../../../images/not-found.png";

export default function KoalaMedia({ koala }) {
    if (!koala)
        return NotFoundImage();
    
    return koala.image ? KoalaImage(koala) : KoalaVideoEmbed(koala);
}

const NotFoundImage = () => 
    <img
        width="100%"
        height="200"
        src={NotFound}
        alt="not-found"
    />

const KoalaImage = (koala) => 
    <img
        src={koala.image.url}
        style={{ width: '100%'}}
        alt="koala"
    />;

const KoalaVideoEmbed = (koala) =>
    <iframe
        width="100%"
        height="400"
        title={`${koala.embedUrl.links?.entries?.block[0]?.title}`}
        src={`${koala.embedUrl.links?.entries?.block[0]?.embedUrl}`}
        frameBorder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen 
    />;
