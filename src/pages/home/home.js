import React, { useEffect, useState } from "react"
import KoalaInformation from "../../components/koalas/koala-information/koala-information";
import KoalaMedia from "../../components/koalas/koala-media/koala-media";
import './home.css'
import Header from "../../components/shared/header/header";
import { useQuery, gql } from '@apollo/client';
import Loader from "../../components/shared/loader/loader";
import { Layout } from "../../components/shared/layout/layout";

const query = gql`
    query KoalaInformation {
        koalaPage(id:"pz3covO7wMwSz8nQgYcg3") {
            title,
            koalasCollection(limit:5) {
                items {
                    title,
                    information,
                    url,
                    linkText,
                    image {
                        url,
                        width,
                        height
                    },
                    embedUrl {
                        links {
                            entries {
                                block {
                                    ... on VideoEmbed {
                                        title,
                                        embedUrl
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`;

export default function HomePage() {
    const [activeKoalaBlock, setActiveKoalaBlock] = useState(null);
    const { data, loading } = useQuery(query);

    useEffect(() => {
        if (data && !activeKoalaBlock) {
            setActiveKoalaBlock(data.koalaPage?.koalasCollection?.items[0]);
        }
    }, [activeKoalaBlock, data])

    return (
        <Layout>
           { loading ? 
                <Loader 
                    message="Fetching contentful data..."
                />  : (
                <>
                    <Header 
                        variant="h1"
                        height="48px"
                        fontSize="32px"
                        fontWeight="600"
                        marginBottom="24px"
                    >
                        {data.koalaPage?.title}
                        </Header>
                    <div className="mediaBlock">
                        <KoalaMedia 
                            koala={activeKoalaBlock}
                        />
                    </div>
                    <div className="informationBlocks">
                        {data.koalaPage?.koalasCollection?.items?.map((koala, index) => 
                            <KoalaInformation 
                                key={index}
                                koala={koala}
                                active={activeKoalaBlock?.title === koala.title}
                                setActive={setActiveKoalaBlock}
                            />
                        )}
                    </div>
                </>
            )}
        </Layout>) 
}